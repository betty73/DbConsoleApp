﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DbConsoleApp.Migrations
{
    public partial class Initial1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, "AlexSmith@gmail.com", "Alex", "Smith" },
                    { 2, "Polly@gmail.com", "Polina", "Ivanova" },
                    { 3, "Petrov@gmail.com", "Ivan", "Petrov" },
                    { 4, "Jpy123@gmail.com", "Petr", "Sidorov" },
                    { 5, "Galla@gmail.com", "Galina", "Oglo" }
                });

            migrationBuilder.InsertData(
                table: "Currencies",
                columns: new[] { "Id", "Label", "Name" },
                values: new object[,]
                {
                    { 1, "RB", "Rubl" },
                    { 2, "TKZ", "Tenge" },
                    { 3, "DUSA", "Dollar" },
                    { 4, "EU", "Euro" },
                    { 5, "LRTRK", "Lira Turkey" }
                });

            migrationBuilder.InsertData(
                table: "Deposits",
                columns: new[] { "Id", "ClientId", "CreatedAt", "CurrencyId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2022, 7, 12, 11, 18, 17, 558, DateTimeKind.Utc).AddTicks(5437), 1 },
                    { 2, 2, new DateTime(2022, 7, 12, 11, 18, 17, 558, DateTimeKind.Utc).AddTicks(5467), 2 },
                    { 3, 3, new DateTime(2022, 7, 12, 11, 18, 17, 558, DateTimeKind.Utc).AddTicks(5474), 3 },
                    { 4, 4, new DateTime(2022, 7, 12, 11, 18, 17, 558, DateTimeKind.Utc).AddTicks(5479), 4 },
                    { 5, 5, new DateTime(2022, 7, 12, 11, 18, 17, 558, DateTimeKind.Utc).AddTicks(5485), 5 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Deposits",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Deposits",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Deposits",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Deposits",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Deposits",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Clients",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
