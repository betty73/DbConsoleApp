﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using System.Reflection;

namespace DbConsoleApp.Model
{
    public class Clients
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\tFirstName: {FirstName}\tLastName: {LastName}\tEmail: {Email}";
        }
    }
}
