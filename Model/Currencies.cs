﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DbConsoleApp.Model
{
    public class Currencies
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Label { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\t Name: {Name}\tLabel: {Label}";

        }
    }
}
