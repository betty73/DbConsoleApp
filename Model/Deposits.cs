﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Emit;
using System.Xml.Linq;

namespace DbConsoleApp.Model
{
    public class Deposits
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("CurrencyId")]
        public Currencies Currencies{get;set;}
        [ForeignKey("ClientId")] 
        public Clients Clients { get; set; }
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return $"Id: {Id}\tCreatedAt: {CreatedAt} \nCurrency: {this.Currencies}\nClient: {Clients} ";
        }
    }
}
