namespace DbConsoleApp;

internal interface IConsoleService
{
    void Run();
}