﻿using System;
using System.IO;
using DbConsoleApp.Data;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using DbConsoleApp;
var host = Host.CreateDefaultBuilder()
    .UseContentRoot(Directory.GetCurrentDirectory())
     .ConfigureLogging(logging =>
     {
         // Add any 3rd party loggers like NLog or Serilog
     })
    .ConfigureAppConfiguration(cfg =>
    {
         cfg.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables();
    })
     .ConfigureServices((_, services) =>
     {
         services.AddDbContext<DbConsoleContext>(options =>
         {
             options.UseNpgsql(_.Configuration.GetConnectionString("DbConsoleContext"));

         })
         .AddScoped<IDbConsoleContext,DbConsoleContext>()
         .AddScoped<IConsoleService,ConsoleService>();
     })
    .Build();
using (var db = host.Services.GetRequiredService<DbConsoleContext>())
{ 
    db.Database.Migrate();
    Console.WriteLine("Applying migrations to Db");
}
var console = host.Services.GetRequiredService<IConsoleService>();
console.Run();



