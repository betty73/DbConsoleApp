﻿using Microsoft.EntityFrameworkCore;
using DbConsoleApp.Model;

namespace DbConsoleApp.Data
{
    internal class DbConsoleContext : DbContext, IDbConsoleContext
    {
        public DbSet<Currencies> Currencies { get; set; }
        public DbSet<Clients> Clients { get; set; }
        public DbSet<Deposits> Deposits { get; set; }
        public DbConsoleContext() { }
        public DbConsoleContext(DbContextOptions<DbConsoleContext> options)
           : base(options)
        {
            // base.Database.Migrate();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Seed();
        }
    }
}
