﻿using DbConsoleApp.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DbConsoleApp.Data
{
    internal interface IDbConsoleContext
    {
        DbSet<Currencies> Currencies { get; set; }
        DbSet<Clients> Clients { get; set; }
        DbSet<Deposits> Deposits { get; set; }
        int SaveChanges();
    }
}
