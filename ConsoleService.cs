﻿using System.Text;
using DbConsoleApp.Data;
using DbConsoleApp.Model;
using Microsoft.EntityFrameworkCore;
using McMaster.Extensions.CommandLineUtils;

namespace DbConsoleApp;

internal class ConsoleService:IConsoleService
{
    private readonly IDbConsoleContext _context;
    private readonly Dictionary<string,string> _tables = new()
    {
        {
            "Clients",
            "Enter with ';': FirstName; LastName; Email"
        },
        {
            "Currencies",
            "Enter with ';': Name; Label"
         },
         {
            "Deposits",
            "Enter with ';': ClientId (int); CurrencyId (int)"
         }
     };
   
    public ConsoleService(IDbConsoleContext context)
    {
        _context = context;
    }
    public void Run()
    {
        do
        {
            var select = Prompt.GetInt("\nChoice Items\n1 - ShowTables\n2 - AddInTables\nChoiceItem:",
                     promptColor: ConsoleColor.Yellow);
            
            switch (select)
            {
                case 1: ShowAllTables(); break;
                case 2: Console.WriteLine(AddInTable()); break;
                default: Console.WriteLine("Use correct choice!"); break;
            }
        }
        while (!Prompt.GetYesNo("Want to exit?", false, ConsoleColor.Yellow));
    }

    private void ShowAllTables()
    {
        var builder = new StringBuilder();
       
        foreach (var table in _tables.Keys)
        {
            builder.Append($"{table}\n");
            switch(table)
            {
                case "Clients":
                    builder.Append(_context.Clients.AsNoTracking()
                   .Select(x => x.ToString())
                   .ToList()
                   .Aggregate((cur, next) => cur + '\n' + next));
                    break;
                case "Currencies":
                    builder.Append(_context.Currencies.AsNoTracking()
                    .Select(x => x.ToString())
                    .ToList()
                    .Aggregate((cur, next) => cur + '\n' + next));
                    break;
                case "Deposits":
                    builder.Append(_context.Deposits.AsNoTracking()
                                .Include(x => x.Currencies)
                                .Include(x => x.Clients)
                                .Select(x => x.ToString())
                                .ToList()
                                .Aggregate((cur, next) => cur + '\n' + next));
                    break;
            }
            builder.AppendLine();
        }
        Console.WriteLine(builder);
    }
    private string AddInTable()
    {
        var builder = new StringBuilder();
        for (var i = 0; i < _tables.Count; i++)
        {
            builder.Append($"{i+1} - {_tables.ElementAt(i).Key}\n");
        }
        var choice = _tables.ElementAt(Prompt.GetInt($"\nChoice Tables\n{builder}ChoiceTable:",
                                           promptColor: ConsoleColor.Yellow)-1);
        Console.WriteLine(choice.Value);
        var data = Console.ReadLine().Split(';').Select(x => x.Trim(' ')).ToList();
        
        switch (choice.Key)
        {
            case "Clients":
                {
                    var record = new Clients()
                    {
                        FirstName = data[0],
                        LastName = data[1],
                        Email = data[2],
                    };
                    _context.Clients.Add(record);
                    _context.SaveChanges();
                    return $"{choice.Key}\n{_context.Clients.OrderBy(x => x.Id).Last()}";
                }

            case "Currencies":
                {
                    var record = new Currencies()
                    {
                        Name = data[0],
                        Label = data[1]
                    };
                    _context.Currencies.Add(record);
                    _context.SaveChanges();
                    return $"{choice.Key}\n{_context.Currencies.OrderBy(x => x.Id).Last()}";
                }
            case "Deposits":
                {
                    var record = new Deposits()
                    {
                        Clients = _context.Clients.FirstOrDefault(x => x.Id == int.Parse(data[0])),
                        Currencies = _context.Currencies.FirstOrDefault(x => x.Id == int.Parse(data[1])),
                        CreatedAt = DateTime.UtcNow
                    };
                    _context.Deposits.Add(record);
                    _context.SaveChanges();
                    return $"{choice.Key}\n{_context.Deposits.OrderBy(x => x.Id).Last()}";
                }

            default: return "Unknown table";
        }
    }
}
